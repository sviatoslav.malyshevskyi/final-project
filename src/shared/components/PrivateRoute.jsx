import React from 'react';
import PropTypes from 'prop-types';
import {Route, Redirect} from 'react-router-dom';

export const PrivateRoute = ({
  component: Component,
  isAuthenticated,
  ...rest
}) => {
  return (
      <Route {...rest} render={props => isAuthenticated ? (
          <Component {...props} />
      ) : (
          <Redirect
            from = {props.location} tp ={{pathname: '/login'}} />
      )}
                       />
  );
};

PrivateRoute.propTypes = {
  component: PropTypes.string.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
};
