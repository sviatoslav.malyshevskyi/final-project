import React from 'react';
import {LogoStyled, TextBack, TextFront} from "./Logo.style";

const Logo = (props) => {
  const {isColored} = props;

  return (
      <div>
        <TextFront isColored = {isColored}>ABC</TextFront>
        <TextBack isColored = {isColored}>f</TextBack>
        <LogoStyled isColored = {isColored} />
        <TextBack isColored = {isColored}>to</TextBack>
      </div>
  );
};

export default Logo;
