import * as Yup from 'yup';
import 'yup-phone';

export function submitStub(values) {
  console.log(JSON.stringify(values));
  alert('STUB\nДанные формы добавлены в консоль');
}

export const nameValidationRules = Yup.string()
    .matches(/^[A-Za-zА-Яа-яіІ]*$/, 'Пожалуйста введите Ваше настоящее имя')
    .min(3, 'Имя не может быть короче 3х символов')
    .max(40, 'Имя не может быть длиннее 40 символов')
    .required('Поле обязательно к заполнению');

export const surnameValidationRules = Yup.string()
    .matches(/^[A-Za-zА-Яа-яіІ]*$/, 'Пожалуйста введите Вашу настоящую фамилию')
    .min(3, 'Фамилия не может быть короче 3х символов')
    .max(40, 'Фамилия не может быть длиннее 40 символов')
    .required('Поле обязательно к заполнению');

export const phoneValidationRules = Yup.string()
    .phone('UA', true, 'Введите действительный номер телефона [UA]')
    .required('Поле обязательно к заполнению');

export const emailValidationRules = Yup.string()
    .email('Введите действительный адрес электронной почты')
    .required('Поле обязательно к заполнению');

export function getMaxBirthday() {
  const currentDate = new Date;
  return `${currentDate.getFullYear() - 18}-${currentDate.getMonth() + 1}-${currentDate.getDate()}`;
}
export const birthdayValidationRules = Yup.date()
    .min('1920-01-01', 'Неужели вы Вампир?')
    .max(getMaxBirthday(), 'Вам должно быть не менее 18ти лет для пользования данным сайтом')
    .required('Поле обязательно к заполнению');

export const genderValidationRules = Yup.string()
    .oneOf(['Мужчина', 'Женщина'], 'Выберите НАИБОЛЕЕ подходящий из предложенных вариантов');

export const passwordPolicy = /.*/;
export const passwordValidationRules = Yup.string()
    .required('Поле обязательно к заполнению')
    .min(8, 'Пароль не может быть меньше 8ми символов')
    .matches(passwordPolicy, 'policy violation [policy shorthand]');

export const dayValidationRules = Yup.number()
    .required('Поле обязательно к заполнению')
    .min(1, 'минимум 1')
    .max(31, 'максимум 31')
    .integer('знаки препинания не разрешены');

export const monthValidationRules = Yup.number()
    .required('Поле обязательно к заполнению')
    .min(1, 'минимум 1')
    .max(31, 'максимум 31')
    .integer('знаки препинания не разрешены');

const currentYear = new Date().getFullYear();
export const yearValidationRules = Yup.number()
    .required('Поле обязательно к заполнению')
    .min(1, 'минимум 1')
    .max(currentYear, `не может быть больше чем ${currentYear}`)
    .integer('знаки препинания не разрешены');
