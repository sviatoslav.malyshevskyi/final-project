import styled from 'styled-components';
import {darken} from 'polished';

export const ActionButtons = styled.div`
  width: 80%;
  margin: 0 auto;
  padding: 15px;
  text-transformation: uppercase;
  text-align: center;
  color: #ffffff;
  background-color: #51AD33;
  border-radius: 5px;
  cursor: pointer;
  transition: 0.3s;
  box-sizing: border-box;
  
  &:hover {
    background-color: $(darken(0.05, #51AD33));
    transition: 0.3s;
  }
`;
