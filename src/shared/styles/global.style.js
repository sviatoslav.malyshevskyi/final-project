import * as React from 'react';
import {createGlobalStyle} from 'styled-components';
import reset from 'styled-reset';
import styled from 'styled-components';
import {Link as ReactLink} from 'react-router-dom';

export const GlobalStyle = createGlobalStyle`
  ${reset}
  margin: 0;
  padding: 0;
  font-family: 'Roboto', sans-serif;
  box-sizing: border-box;
  user-select: none;
`;

export const MainWrapper = styled.div`
  display: flex;
  min-height: 100vh;
  justify-content: space-between;
  flex-direction: column;
`;

export const FullWidthContainer = styled.div`
  max-width: 1200px;
  margin: 0 auto;
  text-align: center;
`;

export const Section = styled.div`
  display: block;
  width: 100%;
  background-color: F7F5F6;
`;

export const Link = styled(ReactLink)`
  display: inline-block;
  padding: 5px 15px;
  font-size: 12px;
  text-decoration: none;
  color: #000000;
  
  &:hover {
    background-color: #B7B7B7;
  }
`;
