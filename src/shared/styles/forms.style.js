import styled from 'styled-components';

export const SampleForm = styled.form`
  display: flex;
  height: 32px;
  padding: 3px;
  align-items: center;
  justify-content: space-between;
  border: 1px solid #B7B7B7;
  background-color: #ffffff;
  border-radius: 20px;
  box-sizing: border-box;
  
  & input {
    margin: 0 10px;
    border: none;
    flex-grow: 1;
  }
  
  & svg {
    width: auto;
    height: 100%;
  }
  
  & circle {
    fill: #B7B7B7;
    transition: 1s;
  }
  
  & focus-within {
    & circle {
      fill: #51AD33;
      transition: 1sl
    }
  }
`;

export const Label = styled.label`
  display: block;
  text-align: left;
`;

export const LabelText = styled.p`
  display: inline-block;
  margin-bottom: 5px;
  font-size: 16px;
  color: #000000;
`;

export const ValidationText = styled(LabelText)`
  color: #DF2725;
  
  &::before {
    content: "- ";
    margin-left: 5px;
  }
`;

const Input = `
  width: 100%;
  margin-bottom: 5px;
  padding: 15px;
  color: #677283;
  border: 1px solid #B7B7B7;
  border-radius: 5px;
  box-sizing: border-box;
  
  &::placeholder {
    color: #B7B7B7;
  }
`;

export const InputText = styled.input`
  ${Input};
`;

export const InputPassword = styled.div`
  position: relative;
  
  input {
    ${Input};
  }
  
  svg {
    position: absolute;
    top: 14px;
    right: 20px;
  }
`;

export const DateInputContainer = styled.div`
  ${Input};
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const Dropdown = styled.select`
  ${Input};
`;
