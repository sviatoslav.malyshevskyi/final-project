import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {productsReducer} from "../../client/products/reducers/productsReducers";

const rootReducer = combineReducers({productsReducer});
const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;
