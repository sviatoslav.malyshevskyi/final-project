import React, {useReducer} from 'react';
import {context} from './context';
import {initialContext} from "./initialContext";
import {SET_CURRENCY, SET_LANG} from "./constants";
import {reducer} from "./reducer";
import PropTypes from 'prop-types';

const ContextProvider = ({children}) => {
  const [state, dispatch] = useReducer(reducer, initialContext);
  const setCurrency = (newCurrency) => {dispatch({type: SET_CURRENCY, payload: newCurrency})};
  const setLanguage = (newLanguage) => {dispatch({type: SET_LANG, payload: newLanguage})};

  return (
      <context.Provider value = {{state, setCurrency, setLanguage}}>
        {children}
      </context.Provider>
  )
}

ContextProvider.propTypes = {
  children: PropTypes.element.isRequired
};

export default ContextProvider;
