import React from 'react';
import {Provider} from 'react-redux';
import {BrowserRouter as Router} from 'react-router-dom';

import {AppRouters, ROUTES} from '../../app.router';
import store from '../../store/store';
import ContextProvider from '../../context/Provider';
import AppRoutes from '../../routes/AppRoutes';
import {GlobalStyle} from "../../../shared/styles/global.style";

function App() {
  return (
      <Provider store={store}>
        <Router>
          <GlobalStyle />
          <ContextProvider>
            <AppRouters routes={ROUTES} />
            <AppRoutes />
          </ContextProvider>
        </Router>
      </Provider>
  );
}

export default App;
