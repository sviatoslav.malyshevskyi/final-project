import React from 'react';
import {Switch, Route, Redirect} from "react-router-dom";
import HomePage from "../client/pages/home/HomePage";

export const ROUTES = [
  {
    exact: true,
    path: '/',
    component: HomePage,
  },
  {
    exact: true,
    path: '/404',
    component: () => <p>404</p>,
  },
  {
    component: (props) => {
      // if (!accessToken) {
      //     redirect
      // }

      return <AppRouters {...props} />
    },
  }
];


const RoutersWithSubRoutes = (route) => (
    <Route path = {route.path} exact = {route.exact} render = {
      (props) => <route.component {...props} routes = {route.routes} />
    } />
);

export const AppRouters = ({routes}) => (
    <Switch>
      {routes.map((route) => (
          <RoutersWithSubRoutes {...route} />
          )
      )}
      <Route path="*">
        <Redirect path="/404"  to={HomePage}/>
      </Route>
    </Switch>
);
