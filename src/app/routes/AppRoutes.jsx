import React from 'react';
import {Switch, Route} from 'react-router-dom';
// import {PrivateRoute} from '../../shared/components/PrivateRoute';
// import useAuth from '../../shared/hooks/useAuth';
import NoMatchPage from "../../client/pages/noMatch";
import HomePage from "../../client/pages/home";

const AppRoutes = () => {
  // const {isAuthenticated} = useAuth();

  return (
      <Switch>
        <Route exact path='/'>
          <HomePage />
        </Route>

        <Route exact path='*'>
          <NoMatchPage />
        </Route>
      </Switch>
  );
};

export default AppRoutes;
