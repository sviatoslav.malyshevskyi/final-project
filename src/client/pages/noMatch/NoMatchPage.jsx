import React from 'react';
import img from './img/404.png';
const NoMatchPage = () => {
  return (
      <section>
        <h2>NO MATCH</h2>
        <img src={img} alt="squashed cans"/>
      </section>
  )
}

export default NoMatchPage;
