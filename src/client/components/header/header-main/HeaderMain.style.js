import styled from 'styled-components';
import {FullWidthContainer} from "../../../../shared/styles/global.style";
import {SampleForm} from "../../../../shared/styles/forms.style";
import SearchLogo from "../main-havbar/SearchLogo";

export const HeaderContainer = styled(FullWidthContainer)`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const HeaderIconsContainer = styled(HeaderContainer)`
  width: 250px;
`;

export const FormHeader = styled(SampleForm)`
  width: 450px;
`;

export const SearchIcon = styled(SearchLogo)`
  width: 41px;
  height: 41px;
  stroke: #ffffff;
  fill: #B7B7B7;
`;
