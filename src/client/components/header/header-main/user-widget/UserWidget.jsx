import React from 'react';
import {useDispatch, connect} from "react-redux";
import {UserWidgetContainer, UserLogo, UserName} from "./UserWidget.style";

const UserWidget = (props) => {
  const {name, isLoggedIn} = props;

  return (
      <UserWidgetContainer test-id={'user'} onClick={() => {
        (isLoggedIn) ? alert('Stub\n Logged in already') : useDispatch(openAuthModal());
      }}>
        <UserLogo test-id={'user-widget-logo'} isLoggedIn={isLoggedIn} />
        <UserName test-id={'user-widget-name'}>{name}</UserName>
      </UserWidgetContainer>
  );
}

const mapStateToProps = () => ({
  isLoggedIn: user.isLoggedIn,
  name: user.name,
});

export default connect(mapStateToProps)(UserWidget);
