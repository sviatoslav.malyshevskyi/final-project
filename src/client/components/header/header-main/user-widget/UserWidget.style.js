import styled from 'styled-components';
import UserImage from "./UserImage";

export const UserWidgetContainer = styled.div`
  display: flex;
  align-items: center;
`;

export const UserLogo = styled(UserImage)`
  width: 41px;
  height: 41px;
  stroke: #ffffff;
  fill: {(props) => props.isLoggedIn ? #51AD33 : #B7B7B7};
`;

export const UserName = styled.h2`
  margin-left: 9px;
  font-size: 16px;
`;
