import React from 'react';
import * as Yup from 'yup';
import {useFormik} from 'formik';
import Logo from "../../../../shared/components/logo/Logo";
import {FullWidthContainer} from "../../../../shared/styles/global.style";
import {submitStub} from "../../../../shared/helpers/form-props/formProps";
import {HeaderIconsContainer, SearchIcon} from "./HeaderMain.style";

const HeaderMain = () => {
  const formik = useFormik ({
    initialValues: {query: ' ',},
    validationSchema: SearchQuerySchema,
    onSubmit: (values, {resetForm}) => submitStub(values, resetForm)
  })

  return (
      <div>
        <FullWidthContainer>
          <Logo isColored />
          <HeaderForm onSubmit={formik.handleSubmit}>
            <input
                type="text"
                name="query"
                value={formik.values.query}
                onChange={formik.handleChange}
                placeholder={Placeholder.Search}
            />
            <SearchIcon onClick={formik.submitForm} />
          </HeaderForm>
          <HeaderIconsContainer>
            {/*<UserWidget />*/}
            {/*<CartWidget />*/}
          </HeaderIconsContainer>
        </FullWidthContainer>
      </div>
  );
};

export default HeaderMain;
