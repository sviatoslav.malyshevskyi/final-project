import styled from 'styled-components';
import CartImage from "./CartImage";

export const CartWidgetContainer = styled.div`
  display: flex;
  position: relative;
  align-items: center;
  justify-content: center;
`;

export const CartLogo = styled(CartImage)`
  position: relative;
  width: 41px;
  height: 41px;
  stroke: #ffffff;
  fill: #B7B7B7;
`;

export const CartWidgetBadge = styled.div`
  position: absolute;
  top: 10px;
  left: -10px;
  padding: 3px 6px;
  color: #ffffff;
  border-radius: 50px;
  background-color: #51ad33;
`;

export const CartText = styled.p`
  margin: 12px 0 0 9px;
  font-size: 16px;
  color: #000000;
`;
