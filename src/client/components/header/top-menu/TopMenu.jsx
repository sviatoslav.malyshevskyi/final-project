import React from 'react';
import {topMenuData} from './topMenu.json';
import {FullWidthContainer, Link, Section} from "../../../../shared/styles/global.style";

const TopMenu = () => {
  return (
      <Section>
        <FullWidthContainer>
          <Link to={topMenuData.href} key={topMenuData.id}>{topMenuData.title}</Link>
        </FullWidthContainer>
      </Section>
  );
};

export default TopMenu;
